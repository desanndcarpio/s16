/*Loops*/

/*
	Display each element available on our array
*/

let students = ["TJ", "Mia", "Tin", "Chris"];

/*
	While Loop
*/

//Repeat a name "Sylvan" 5x

	let count = 5; //number of iterations, times to repeat our code

	while(count !== 0){ 
		console.log("Sylvan");
		count--; //will be decremented by 1
	}


//Print numbers 1 to 5 

let number = 1;

while(number <= 5){
	console.log(number);
	number++;
}

//Loop Array

/*
	Mini Activity
*/

let fruits = ["Banana","Mango","Apple","Grapes"];
let i = 0;  //reference to the index number of the given array - array starts w/ 0

while (fruits[i]){
	console.log(fruits[i]);
	i++;
}

/*let fruits = ["Banana","Mango"];
let i = 0;  //reference to the index number of the given array - array starts w/ 0

while (i <= 1){ //need to know the last index position
	console.log(fruits[i]);
	i++;
}*/
/***********************************************************/
// getting the last index position of array w/o manually counting

let mobilePhones = ['Samsung Galaxy S21', 'Iphone 13 Pro', 'Xiaomi 11T', 'Realme C', 'Huawei Nova 8', 'Pixel 5', 'Asus Rouge 6','Nokia','Cherry Mobile'];

console.log(mobilePhones.length); // gives count of the elements in the array
console.log(mobilePhones.length - 1); // gives the last index position of an element inside an array
console.log(mobilePhones[mobilePhones.length -1]); // last element of the array

let indexNumberForMobile = 0; 

while (indexNumberForMobile <= mobilePhones.length -1){
	console.log(mobilePhones[indexNumberForMobile]);
	indexNumberForMobile++;
}

/****************************************************************/

/*
	Do While

*/

let countA = 1;

do {
	console.log("Juan");
	countA++;
} while (countA <= 6);

/*
	Do-While VS While

*/

let countB = 6;

do {
	console.log(`Do-While count ${countB}`);
	countB--;
} while (countB == 7);

//Versus

	// will not print in console condition is false 
while(countB == 7){
	console.log(`While count ${countB}`);
	countB--;
}

/*
	Mini Activity
*/

let indexNumberA = 0;
let computerBrands = ['Apple Macbook Pro', 'HP Notebook', 'Asus', 'Lenovo', 'Acer', 'Dell', 'Huawei'];

do {
	console.log(computerBrands[indexNumberA]);
	indexNumberA++;
} while (indexNumberA <= computerBrands.length-1);

/*************************************************************/

/*
	For Loop
*/
	//variable is within the for loop only
	//condition check if true or false
	//itertion

for(let count=5; count >= 0; count--){
	console.log(count);
}

// Mini-Activty

let colors = ['Red','Green', 'Blue','Yellow','Purple', 'White', 'Black'];

for(let i=0; i <= colors.length -1; i++){
	console.log(colors[i]);
}


/***********************************************************/

/*
	Continue & Break

*/

//Continue

/*
ages	
	18, 19, 20, 21, 24, 25

	age == 21 (debut age of boys), we will skip then go to the next iteration

	18, 19, 20, 24, 25

*/

let ages = [18, 19, 20, 21, 24, 25];

//skip the debutante age boys and girls using continue 

for(let i=0; i <= ages.length - 1; i++){
	if(ages[i] == 21 || ages[i] == 18){
		continue;
	}
	console.log(ages[i]);
}


//Break

/*
	let studentNames = ['Den', 'Jayson', 'Marvin', 'Rommel'];
	
	Once we find Jayson on our array, we will stop the loop

	Den
	Jayson 
*/

let studentNames = ['Den', 'Jayson', 'Marvin', 'Rommel'];

for (let i = 0; i <= studentNames.length - 1; i++){
	if(studentNames[i] == "Jayson"){
		console.log(studentNames[i]);	
		break;
	}
	console.log(studentNames[i]);
}
/*******************************************************/
console.log('*********************************')

//Coding Challenge - Scope (Loops, Continue and Break)
// You can add the solution under our s16/d1/index.js only, no need to create a separate folder for this. Kindly push your solutions once you are done
//Then link your activity gitlab repo under Boodle WD078-16

/*
Instructions:

1. Given the array 'adultAge', display only adult age range on the console.

-- the goal of this activity is to exclude values that are not in the range of adult age. Using the tool loops, continue or break, 
the students must display only the given sample output below on their console.
*/
let adultAge = [20, 23, 33, 27, 18, 19, 70, 15, 55, 63, 85, 12, 19];

/*
	Sample output:
  
  20
  23
  33
  27
  70
  55
  63
  85
*/

for(let i = 0; i <= adultAge.length - 1; i++){
	if(adultAge[i] <= 19 ){
		continue;
	}
	console.log(adultAge[i]);
};


/*
Instructions:

2. Given an array 'students' and a function searchStudent, create a solution that, once a function is invoked with a given name of student as its argument,
	the function will start to loop and search for the student on a given array and once there's found it will print it on the console 
  and stop the execution of loop. 
  
  -- the goal of this activity is to print only the value needed based on the argument given. Use loop, and continue or break
*/
let students1 = ['Gary', 'Amelie', 'Anne', 'Jazz', 'Preina', 'James', 'Kelly', 'Diane', 'Lucy', 'Vanessa', 'Kim', 'Francine'];

function searchStudent(studentName){
	for (let i = 0; i <= students1.length - 1; i++ ){
		if(students1[i] == 'Jazz'){
			console.log(students1[i]);
			break;
		}
	}
}

searchStudent('Jazz'); //invoked function with a given argument 'Jazz'

/*
	Sample output:
  
  Jazz
*/